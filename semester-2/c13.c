#include <stdio.h>
#include <string.h>

int main()
{
    int T;
    int i;
    char s[101];
    scanf("%d", &T);

    while(T--){
        scanf("%s", s);
        int L = strlen(s);

        for(i=0;i<=L/2-1;i++){
            if(s[i]!=s[L-1-i]) break;

        }

        if(i==L/2)
            printf("YES\n");
        else
            printf("No\n");
    }
}
