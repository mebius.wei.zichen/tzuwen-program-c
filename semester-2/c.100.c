#include <stdio.h>
#include <string.h>

int main()
{
    int N, temp, count=0, d[5]={25, 20, 10, 5, 2};
    scanf("%d", &N);

    for(int j=0;j<5;j++){
        temp=N;
        for(int i=j;;i<5){
            if(temp-d[i]<0){
                i++;
            }
            if(temp>=d[i]){
                temp-=d[i];
            }

            if(temp==0){
                count++;
                printf("%d ", temp);
                break;
            }
        }
    }
    printf("%d ", count);
}
