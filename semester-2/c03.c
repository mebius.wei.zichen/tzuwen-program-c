#include <stdio.h>
#include <string.h>

int main()
{
   int T;
   char A[1025];
   char B[1025];
   scanf("%d", &T);

   while(T--){
        scanf("%s", A);
        scanf("%s", B);
        int L = strlen(A);
        for(int i=0;i<L;i++){
            if(A[i]=='?' && B[i]=='?'){
                printf("No\n");
                break;
            }
            else if(A[i]=='?' && B[i]!='?')
                A[i]=B[i];

            if(i==L-1)
                printf("Yes:%s\n", A);
        }
   }
}
