#include <stdio.h>
#include <stdlib.h>

int main()
{
    int T;
    int M, N;
    long long sum=0;
    scanf("%d", &T);

    while(T--){
        scanf("%d %d", &M, &N);
        sum+=N*(1+N)/2;
        sum+=(N/M-1)*(N/M)/2*M;
        sum+= (N%M)*(N/M);
        printf("%lld", sum);
    }
}
