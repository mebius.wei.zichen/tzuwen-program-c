#include <stdio.h>
#include <stdlib.h>

int main()
{

    int T;
    scanf("%d", &T);

    while(T--){
        int N;
        int M, x1=0, x2;
        int right=0, left=0;
        scanf("%d", &N);
        for(int i=0;i<N;i++){
            scanf("%d", &M);
            x2 = x1+M;
            if(M>0){
              if(x1>=0)
                    right+=M;

              else if(x1<0 && x2<0)
                    left+=M;

              else{
                  right+=(x2-0);
                  left+=(0-x1);
              }
            }
            else{
                if(x1<=0)
                    left+=-M;

                else if(x1>0 && x2>0)
                    right+=-M;

                else{
                    right+=(x1-0);
                    left+=(0-x2);
                }
            }
            x1 = x2;
        }
        if(right==left)
            printf("Both okay\n");
        else if(right>left)
            printf("Go right\n");
        else
            printf("Go left\n");
    }
}
