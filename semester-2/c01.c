#include <stdio.h>
#include <stdlib.h>

int main()
{
    long long AB, H, BB, HBP, TB, SF;
    int N;
    scanf("%d", &N);

    while(N--){
        scanf("%lld %lld %lld %lld %lld %lld", &AB, &H, &BB, &HBP, &TB, &SF);
        long long A = AB*(H+BB+HBP)+TB*(AB+BB+SF+HBP);
        long long B = AB*(AB+BB+SF+HBP);
        double OPS = (double)A/(double)B;
        printf("%.3lf\n", OPS);
    }
}
