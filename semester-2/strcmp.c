#include <stdio.h>
#include <string.h>

int main() {
  // char str1[] = "abcd", str2[] = "abCd", str3[] = "abcd";
  char str1[3] = {0}, str2[3] = {0};
  int result;
  gets(str1);
  gets(str2);
  // comparing strings str1 and str2
  result = strcmp(str1, str2);
  printf("str1=%s\n", str1);
  printf("str2=%s\n", str2);
  printf("strcmp(str1, str2) = %d\n", result);


  return 0;
}
