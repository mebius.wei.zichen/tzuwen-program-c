#include<iostream>

using namespace std;

int * getArray(int n)
{
    int *p = new int[n];

    for(int i=0;i<n;i++)
        p[i]=i;

    return p;
}

int main()
{
    int n=10;
    int *A=getArray(n);

    for(int i=0;i<n;i++)
        cout<<A[i]<<endl;

    return 0;
}
