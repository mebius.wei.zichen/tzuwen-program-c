#include<iostream>

using namespace std;

struct Rectangle
{
    int lenght;
    int width;
};

int main()
{
    Rectangle r={100,200};

    cout<<r.lenght<<endl;
    cout<<r.width<<endl;

    Rectangle  *p=&r;

    cout<<p->lenght<<endl;
    cout<<p->width<<endl;

    Rectangle  *n;
    //n=(struct Rectangle *)malloc(sizeof(struct Rectangle));   // For C
    n=new Rectangle(); // For C++
    n->lenght = 50;
    n->width = 20;

    cout<<n->lenght<<endl;
    cout<<n->width<<endl;

}
