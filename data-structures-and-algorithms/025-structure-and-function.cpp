#include<iostream>

using namespace std;

struct Rectangle
{
    int length=0;
    int width=0;
};

void init(Rectangle *r, int length, int width)
{
    r->length=length;
    r->width=width;
}

int getArea(Rectangle r)
{
    return r.length*r.width;
}

int getPerimter(Rectangle r)
{
    return 2*(r.length+r.width);
}

int main()
{
    int length, width, area, peri;
    Rectangle r={0,0};

    cin>>length>>width;

    init(&r,length, width);
    area=getArea(r);
    peri=getPerimter(r);

    cout<<area<<","<<peri<<endl;
}
