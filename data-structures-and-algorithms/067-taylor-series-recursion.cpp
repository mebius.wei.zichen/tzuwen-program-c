#include <iostream>

using namespace std;

double p = 1, f = 1;

double e(int x, int n)
{
    double r;

    if (n == 0)
        return 1;

    r = e(x, n - 1);
    // cout << r << endl;
    p = p * x;
    f = f * n;

    return r + p / f;
}

int main()
{
    double r = e(3, 3);

    cout << r << endl;

    return 0;
}