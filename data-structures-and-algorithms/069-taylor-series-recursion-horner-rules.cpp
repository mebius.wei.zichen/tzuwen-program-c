#include <iostream>

using namespace std;

double s;

double e(int x, int n)
{
    if (n == 0)
        return s;

    s = 1 + x * s / n;

    return e(x, n - 1);
}

int main()
{
    double r = e(3, 4);

    cout << r << endl;

    return 0;
}