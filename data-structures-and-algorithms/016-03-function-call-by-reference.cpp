#include<iostream>

using namespace std;

void swap(int &x, int &y)
{
    int temp=x;
    x=y;
    y=temp;
}

int main()
{
    int num1=10;
    int num2=20;

    cout<<num1<<","<<num2<<endl;
    swap(num1, num2);
    cout<<num1<<","<<num2<<endl;

    return 0;
}
