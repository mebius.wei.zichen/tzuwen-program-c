#include <iostream>

using namespace std;

int sum(int n)
{
    if (n == 0)
        return 0;
    return sum(n - 1) + n;
}

int Isum(int n)
{
    int s = 0, i;
    for (i = 1; i <= n; i++)
        s = s + i;
    return s;
}

int main()
{
    int r1 = Isum(3);
    int r2 = Isum(5);

    cout << r1 << endl;
    cout << r2 << endl;

    return 0;
}