#include<iostream>

using namespace std;

int getArea(int length, int width)
{
    return length*width;
}

int getPerimter(int length, int width)
{
    return 2*(length+width);
}

int main()
{
    int length=0;
    int width=0;
    int area=0;
    int peri=0;

    length=10;
    width=20;

    area=getArea(length, width);
    peri=getPerimter(length, width);

    cout<<area<<","<<peri<<endl;
}
