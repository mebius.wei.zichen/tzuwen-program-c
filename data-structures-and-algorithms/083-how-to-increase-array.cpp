#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
    int A[3][4] = {{1, 2, 3, 4},
                   {5, 6, 7, 8},
                   {9, 10, 11, 12}};

    int *B[3];
    int **C;
    int i, j;

    B[0] = new int[4]{10, 20, 30, 40};
    B[1] = new int[4]{50, 60, 70, 80};
    B[2] = new int[4]{90, 100, 110, 120};

    C = new int *[3];
    C[0] = new int[4]{100, 200, 300, 400};
    C[1] = new int[4]{500, 600, 700, 800};
    C[2] = new int[4]{900, 1000, 1100, 1200};

    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            cout << A[i][j] << endl;
        }
    }

    cout << "======" << endl;

    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            cout << B[i][j] << endl;
        }
    }

    cout << "======" << endl;

    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            cout << C[i][j] << endl;
        }
    }

    cout << "======" << endl;

    return 0;
}