#include <iostream>

using namespace std;

class Rectangle
{
private:
    int _length;
    int _width;

public:
    Rectangle()
    {
        _length = 0;
        _width = 0;
    }

    Rectangle(int length, int width)
    {
        _length = length;
        _width = width;
    }

    int getArea()
    {
        return _length * _width;
    }

    int getPerimter()
    {
        return 2 * (_length + _width);
    }

    void setLength(int length)
    {
        _length = length;
    }

    void setWidth(int width)
    {
        _width = width;
    }

    ~Rectangle()
    {
        cout << "Destructor" << endl;
    }
};

int main()
{
    int length, width, area, peri;

    cin >> length >> width;

    Rectangle r(length, width);

    area = r.getArea();
    peri = r.getPerimter();

    cout << area << "," << peri << endl;
}
