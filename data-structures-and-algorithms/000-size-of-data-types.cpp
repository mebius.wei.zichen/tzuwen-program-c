// C++ program to sizes of data types
#include<iostream>
using namespace std;

int main()
{
    cout << "char : " << sizeof(char)
      << " byte" << endl;
    cout << "int : " << sizeof(int)
      << " bytes" << endl;
    cout << "short int : " << sizeof(short int)
      << " bytes" << endl;
    cout << "long int : " << sizeof(long int)
       << " bytes" << endl;
    cout << "signed long int : " << sizeof(signed long int)
       << " bytes" << endl;
    cout << "unsigned long int : " << sizeof(unsigned long int)
       << " bytes" << endl;
    cout << "float : " << sizeof(float)
       << " bytes" <<endl;
    cout << "double : " << sizeof(double)
       << " bytes" << endl;
    cout << "wchar_t : " << sizeof(wchar_t)
       << " bytes" <<endl;

    return 0;
}
