#include<iostream>
#include <stdio.h>

using namespace std;

int main()
{
    int *p;

    p=new int[5] {0,1,2,3,4}; //C++ syntax

    for(int i=0; i<5; i++)
    {
        cout<<p[i]<<endl;
    }

    delete [] p; // C++
    //free(p); // C

    return 0;
}
