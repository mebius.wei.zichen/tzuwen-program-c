#include<iostream>

using namespace std;

// Array is always call by address
// void fun(int *A, int n)
void fun(int A[], int n)
{
    for(int i=0;i<n;i++)
        A[i]=A[i]*10;
}

int main()
{
    int A[]={1,2,3,4,5};
    int n=5;

    fun(A, 5);

    for(int x:A)
        cout<<x<<endl;

    return 0;
}
