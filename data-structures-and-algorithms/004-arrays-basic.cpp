#include<iostream>

using namespace std;

int main()
{
    int A[5]={10,11};
    A[2]=13;
    A[3]=14;
    A[4]=15;

    cout<<"sizeof(A[0]): "<<sizeof(A[0])<<" Bytes"<<endl;
    cout<<endl;

    for(int i=0; i<10; i++)
    {
        cout<<"A["<<i<<"]="<<A[i]<<endl;
    }

    cout<<endl;

    for(int x:A)
    {
        cout<<x<<endl;
    }

    return 0;
}
