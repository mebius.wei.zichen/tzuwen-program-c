#include <iostream>

using namespace std;

template <class T>
class Arithmetic
{
private:
    T num1;
    T num2;

public:
    Arithmetic(T num1, T num2);
    T add();
    T sub();
};

template <class T>
Arithmetic<T>::Arithmetic(T num1, T num2)
{
    this->num1 = num1;
    this->num2 = num2;
}

template <class T>
T Arithmetic<T>::add()
{
    return num1 + num2;
}

template <class T>
T Arithmetic<T>::sub()
{
    return num1 - num2;
}

int main()
{

    Arithmetic<char> ar2('a', 1);
    cout << "ar2.add() = " << ar2.add() << endl;
    cout << "ar2.sub() = " << ar2.sub() << endl;
}
