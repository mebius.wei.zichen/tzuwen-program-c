#include<iostream>

using namespace std;

struct Rectangle
{
    int lenght;
    int width;
};

void callByValue(Rectangle r)
{
    r.lenght=r.lenght+1;
    r.width=r.width+1;

    cout<<r.lenght<<","<<r.width<<endl;
}

void callByAddress(Rectangle *r)
{
    r->lenght=r->lenght+2;
    r->width=r->width+2;

    cout<<r->lenght<<","<<r->width<<endl;
}

int main()
{
    Rectangle r={100,200};

    callByValue(r);

    cout<<r.lenght<<","<<r.width<<endl;

    callByAddress(&r);

    cout<<r.lenght<<","<<r.width<<endl;
}
