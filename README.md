## Add ssk key to git repo

```bash
git remote set-url origin git@gitlab.com:mebius.wei.zichen/tzuwen-program-c.git
```

## cpp version

- 高中生解題系統

  - g++ -std=c++14 (g++ 7.4.0)

- APCS (可使用 standard libraries)

  - g++ -g -O2 -std=gnu++11 -static -lm -o <a.out>

![](images/2021-06-26-11-12-45.png)

![](images/2021-06-26-11-26-47.png)

![](images/2021-06-26-11-51-11.png)

- NPSC (可使用 standard libraries)

  - server: gcc 9.2.1 (Ubuntu 9.2.1-9ubuntu2~16.04.1)
  - client: msys2 gcc 9.1.0 64bit 以上

  - Kattis
    ![](images/2021-06-26-11-45-23.png)
