#include <stdio.h>
#include <stdlib.h>

void p3()
{
    int i;
    for(i=1;i<=3;i++){
        printf("%d\n", i);
    }
}

void p(int num)
{
    int i;
    for(i=1;i<=num;i++){
        printf("%d\n", i);
    }
}

int rect_area(int length, int width)
{
    int area = length*width;
    return area;
}

int main()
{
   p3();
   printf("-------\n");

   p(6);
   printf("-------\n");

   p(3);
   printf("-------\n");

   int a = rect_area(10,5);
   printf("%d\n", a);

   return 0;
}
