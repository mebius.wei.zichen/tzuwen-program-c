#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int MyRand(int min, int max)
{
    int r = rand()%(max-min+1)+min;
    return r;
}

int main()
{
    srand(time(NULL));

    int x;
    int a, b;
    scanf("%d", &x);

    if(x<=1000)
        a=MyRand(0, x);
    else
        a=MyRand(x-1000, 1000);

    b=x-a;
    printf("%d %d", a, b);
}
