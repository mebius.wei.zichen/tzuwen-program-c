#include <stdio.h>
#include <string.h>

int main()
{
    int P[6], R[6];
    int max = 0;

    for(int i=0;i<6;i++){
        scanf("%d %d", &P[i], &R[i]);
        if(P[i]>max)
            max = P[i];
    }

    if(max<6)
        printf("Yes");
    else if(P[0]==6 && R[0]==1)
        printf("Yes");
    else
        printf("No");

}
