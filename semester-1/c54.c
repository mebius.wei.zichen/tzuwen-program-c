#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n, a, b;
    int day = 0;
    scanf("%d %d %d", &n, &a, &b);

    if(a-b<=0){
        printf("-1");
        return 0;
    }

    while(1){
        if(n-a<=0){
            day++;
            break;
        }
        if(n-(a-b)>=0){
            n = n-(a-b);
            day++;
        }
        else
            break;
    }
    printf("%d", day);
}
