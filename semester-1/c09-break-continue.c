#include <stdio.h>
#include <stdlib.h>

int main()
{

    while(1){
        int num;
        printf("Please input number(Enter 0 to end):");
        scanf("%d", &num);

        if(num == 0)
            break;
        else if(num%2==0){
            printf("%d is a multiple of two\n", num);
            continue;
        }
        else if(num%3==0){
            printf("%d is a multiple of three\n", num);
            continue;
        }
        printf("%d isn't multiple of two and three\n", num);
    }
}
