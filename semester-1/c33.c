#include <stdio.h>
#include <stdlib.h>

int Fibonacci(int n)
{
    if(n==1 || n==2)
        return 1;
    else
        return Fibonacci(n-1) + Fibonacci(n-2);
}

int Fibonacci_loop(int n)
{
    if(n==1 || n==2)
        return 1;
    else{
        int i;
        int a1 = 1;
        int a2 = 1;
        int a3;
        for(i=3;i<=n;i++){
            a3 = a1 + a2;
            a1 = a2;
            a2 = a3;
        }
        return a3;
    }

}

int main()
{
    int n = 8;

    int answer = Fibonacci(n);
    printf("%d", answer);
    printf("\n------------\n");

    int total = Fibonacci_loop(n);
    printf("%d", total);
}
