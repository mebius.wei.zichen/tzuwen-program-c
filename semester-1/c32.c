#include <stdio.h>
#include <stdlib.h>

int add_1to(int n)
{
    if(n==1)
        return 1;
    else
        return add_1to(n-1) + n;
}

int main()
{
    int sum=0;
    for(int i=1;i<=4;i++){
        sum += i;
    }

    printf("%d\n", sum);
    printf("-------------\n");

    sum = add_1to(4);
    printf("%d", sum);
}
