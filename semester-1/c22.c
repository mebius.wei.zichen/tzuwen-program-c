#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i;
    i = 0;

    //int score[5];
    //score[0] = 90;
    //score[1] = 85;
    //score[2] = 100;
    //score[3] = 80;
    //score[4] = 70;
    //int score[5] = {90, 85, 100, 80, 70};
    int score[5] = {0};
    //int score[5] = {100};

    for(i=0;i<5;i++){
        printf("%d ", score[i]);
    }

    printf("\n-------------------\n");

    for(i=0;i<5;i++){
        scanf("%d", &score[i]);
    }

    printf("-------------------\n");

    for(i=0;i<5;i++){
        printf("%d ", score[i]);
    }

    int max = -2147483648;
    printf("\n");
    printf("Maximum of five numbers:");
    for(i=0;i<5;i++){
        if(score[i]>max)
            max = score[i];
    }
    printf("%d\n", max);
}
