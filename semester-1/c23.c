#include <stdio.h>
#include <stdlib.h>

int power(int b, int n)
{
    int r = 1;
    for(int i=1;i<=n;i++){
        r=r*b;
    }

    return r;
}
int main()
{
    int b, n;
    scanf("%d %d", &b, &n);
    int r = power(b,n);

    printf("%d", r);

    return 0;
}
