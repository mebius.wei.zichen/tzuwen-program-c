#include <stdio.h>
#include <string.h>
int main()
{
    int N, M;
    scanf("%d %d", &N, &M);

    int w[N];
    int d[M];
    int max=0;
    int min=10000;
    int T = N;
    long long count = 0;

    for(int i=0;i<N;i++)
        scanf("%d", &w[i]);
    for(int i=0;i<M;i++)
        scanf("%d", &d[i]);

    while(T--){

        for(int i=0;i<N;i++){
            if(w[i]>=max){
                max = w[i];
            }
        }

        for(int i=0;i<N;i++){
            if(w[i]==max){
                w[i] = 0;
                break;
            }
        }

        for(int i=0;i<M;i++){
            if(d[i]<=min){
                min = d[i];
            }
        }

         for(int i=0;i<M;i++){
            if(d[i]==min){
                d[i] = 10000;
                break;
            }
        }

        count = count+min*max;
        max = 0;
        min = 10000;

    }
    printf("%lld", count);
}
