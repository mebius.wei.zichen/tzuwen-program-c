#include <stdio.h>
#include <string.h>

int Recursion(int a, int b)
{
    int r = a % b;

    if(r == 0)
        return b;
    else
        return Recursion(b ,r);
}

int main()
{

    int a, b ;
    printf("Please input two numbers:");
    scanf("%d %d", &a, &b);

    int result = Recursion(a, b);

    printf("%d", result);
}
