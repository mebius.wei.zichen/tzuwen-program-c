#include <stdio.h>
#include <string.h>

int main()
{
    int T;
    int N, M, k;
    int get;
    scanf("%d", &T);

    while(T--){
        scanf("%d %d %d", &N, &M, &k);
        get = M/N;
        if(M%N>=k)
            get++;

        printf("%d\n", get);
    }
    return 0;
}
