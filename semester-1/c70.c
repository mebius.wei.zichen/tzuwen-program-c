#include <stdio.h>
#include <stdlib.h>

int main()
{
    int N;
    int rank;
    int count = 0;
    scanf("%d", &N);

    while(N--){
        scanf("%d", &rank);
        if(rank<=25)
            count++;
    }

    if(count>=5)
        printf("5");
    else
        printf("%d", count);
}
