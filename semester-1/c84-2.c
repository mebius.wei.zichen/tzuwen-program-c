#include <stdio.h>
#include <stdlib.h>
int cmp1(const void* a, const void* b)
{
    if(*(int*)a < *(int*)b)
        return 1;
    else if(*(int*)a == *(int*)b)
        return 0;
    else
        return -1;
}

int cmp2(const void* a, const void* b)
{
    if(*(int*)a < *(int*)b)
        return -1;
    else if(*(int*)a == *(int*)b)
        return 0;
    else
        return 1;
}

int main()
{
    int N, M;
    long long sum = 0;
    scanf("%d %d", &N, &M);

    int w[N];
    int d[M];

    for(int i=0;i<N;i++)
        scanf("%d", &w[i]);
    for(int i=0;i<M;i++)
        scanf("%d", &d[i]);

    qsort(w, N, sizeof(int), cmp1);
    qsort(d, M, sizeof(int), cmp2);

    for(int i=0;i<N;i++){
        sum+=w[i]*d[i];
    }
    printf("%lld", sum);
}
