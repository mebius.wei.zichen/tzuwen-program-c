#include <stdio.h>
#include <string.h>

int main()
{
    int M, N;
    scanf("%d %d", &M, &N);

    int ans = N/M;
    if(N%M!=0)
        ans++;
    printf("%d", ans);
}
