#include <stdio.h>
#include <stdlib.h>

int Pascal(int M, int N)
{
    if(N==1 || M==N)
        return 1;
    else
        return Pascal(M-1, N-1) + Pascal(M-1, N);
}

int main()
{
    int M, N, result;
    int T;
    scanf("%d", &T);

    while(T--){
        scanf("%d %d", &M, &N);
        result = Pascal(M, N);
        printf("%d\n", result);
    }
}
