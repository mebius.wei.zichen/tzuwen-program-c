#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n[10];
    int i;
    for(i=0;i<10;i++){
        scanf("%d", &n[i]);
    }

    int swapped;
    int end = 9;

    do{
        swapped = 0;
        for(i=0;i<=end;i++){
            if(n[i]>n[i+1]){
                int temp = n[i+1];
                n[i+1] = n[i];
                n[i] = temp;

                swapped = 1;
            }
        }
        end--;
    }while(swapped == 1);

    for(i=0;i<10;i++){
        printf("%d ", n[i]);
    }


}
