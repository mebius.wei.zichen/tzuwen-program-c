#include <stdio.h>
#include <stdlib.h>

int main()
{
    int pl, pa, pd;
    int gl, ga, gd;
    int N = 0;
    while(1){
        scanf("%d %d %d %d %d %d", &pl, &pa, &pd, &gl, &ga, &gd);
        if(pl+pa+pd+gl+ga+gd==0)
            return 0;

        pa-=gd;
        ga-=pd;

        while(1){
            N++;
            pl-=ga;
            gl-=pa;
            if(gl<=0){
                printf("You win in %d round(s).\n", N);
                N = 0;
                break;
            }
            else if(pl<=0){
                printf("You lose in %d round(s).\n", N);
                N = 0;
                break;
            }
        }
    }
}
