#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i;
    int sum=0;
    printf("1+2+3+4=");

    for(i=1;i<5;i++){
        sum+=i;
    }
    printf("%d", sum);

    sum = 1;
    printf("\n1*2*3*4=");

    for(i=1;i<5;i++){
        sum*=i;
    }
    printf("%d", sum);
}
