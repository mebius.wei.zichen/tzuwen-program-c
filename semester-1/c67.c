#include <stdio.h>
#include <string.h>

int main()
{
    int a[3][4];
    int b[4][3];
    int c[3][4];
    int d[4][3];

    int num = 1;

    for(int i=0;i<3;i++){
        for(int n=0;n<4;n++){
            scanf("%d", &a[i][n]);
            num++;
        }
    }

    printf("\n");

    for(int i=0;i<3;i++){
        for(int n=0;n<4;n++)
            b[n][2-i] = a[i][n];
    }
    for(int i=0;i<3;i++){
        for(int n=0;n<4;n++)
            c[2-i][3-n] = a[i][n];
    }
    for(int i=0;i<3;i++){
        for(int n=0;n<4;n++)
            d[3-n][i] = a[i][n];
    }

    for(int i=0;i<4;i++){
        for(int n=0;n<3;n++)
            printf("%02d ", b[i][n]);

        printf("\n");
    }

    printf("\n");

    for(int i=0;i<3;i++){
        for(int n=0;n<4;n++)
            printf("%02d ", c[i][n]);

        printf("\n");
    }

}

