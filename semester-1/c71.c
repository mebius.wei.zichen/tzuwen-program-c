#include <stdio.h>
#include <stdlib.h>

int main()
{
    int T;
    long long year;

    scanf("%d", &T);
    while(T--){
        scanf("%lld", &year);
        printf("%lld\n", year);
    }
}
