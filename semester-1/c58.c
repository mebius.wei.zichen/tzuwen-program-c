#include <stdio.h>
#include <stdlib.h>

int LCM(int m, int k)
{
    int r = m % k;
    if(r == 0)
        return k;
    else
        return LCM(k, r);
}

int main()
{
    int m, k;
    int n;

    scanf("%d", &n);

    while(n--){
        scanf("%d %d", &m, &k);
        int result = m*k/LCM(m, k);

        printf("%d\n", result);
    }

}
