#include <stdio.h>
#include <string.h>

int main()
{
    int n;
    int x, y;

    scanf("%d", &n);
    scanf("%d %d", &x, &y);

    if(n==x)
        printf("%d", y);
    else if(n==y)
        printf("%d", x);
    else
        printf("%d", n);
}
