#include <stdio.h>
#include <stdlib.h>

int main()
{
    int max, min;
    int temp;
    printf("Please input numbers:");
    scanf("%d %d", &max, &min);

    if(max<min){
        temp = min;
        min = max;
        max = temp;
    }

    printf("max =  %d    min = %d", max, min);
}
