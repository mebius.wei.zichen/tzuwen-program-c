#include <stdio.h>
#include <string.h>

int main()
{
    char cards[13][3] = {"1z", "2z", "3z", "4z", "5z", "6z", "7z",
                        "1m", "9m", "1p", "9p", "1s", "9s"};

    int card[13][3];
    int count = 0;

    for(int i=0;i<13;i++){
        scanf("%s", card[i]);
        for(int n=0;n<13;n++){
            if(strcmp(card[i], cards[n])==0){
                count++;
                break;
            }
        }
    }
    if(count==13){
        count = 0;
        for(int i=0;i<12;i++){
            for(int n=i+1;n<13;n++){
                if(strcmp(card[i], card[n])==0)
                    count++;
            }
        }
        if(count==0)
            printf("13");
        else if(count==1)
            printf("1");
        else
            printf("0");
    }
    else
        printf("0");

}

