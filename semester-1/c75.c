#include <stdio.h>
#include <string.h>

int main()
{
    int T;
    int hour, second;
    scanf("%d", &T);
    while(T--){
        scanf("%d %d", &hour, &second);
        second += 30;
        if(second>=60){
            hour++;
            second -=60;
            if(hour==24)
                hour=0;
        }
        printf("%d %d\n", hour, second);
    }
}
