#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num;
    printf("Please input number:");
    scanf("%d", &num);

    for(int i=1;i<=num;i++){
        if(num%i==0)
            printf("\n = %d * %d", i, num/i);
    }
}
