#include <stdio.h>
#include <string.h>

int main()
{
   int N;
   scanf("%d", &N);

   char S[N+1];
   scanf("%s", S);

   int K;
   scanf("%d", &K);

   int count;
   for(int i=1;i<=N;i++){
        if(S[i-1]!='C' && i!=K){
            if(count==0){
                printf("%d", i);
                count++;
            }
            else{
                printf(" %d", i);
                count++;
            }
        }
        if(count>=N-2)break;
   }
}
