#include <stdio.h>

int main()
{
    long long a[9];

    for(int i=0;i<9;i++){
        scanf("%lld", &a[i]);
    }

    long long sum1 = a[1]+a[4]+a[7];
    long long sum2 = a[0]+a[1]+a[2];
    printf("%lld", (sum1-sum2)/2);

    return 0;
}
