#include <stdio.h>
#include <string.h>

int main()
{
    int N, M;
    int A, B;
    int x;
    scanf("%d %d %d %d", &N, &M, &A, &B);

    while(M--){
        scanf("%d", &x);
        if(A<B){
            if(x>A && x<B){
                A++;
                B--;
            }
            else{
                A--;
                B++;
            }
        }
        else{
            if(x<A && x>B){
                A--;
                B++;
            }
            else{
                A++;
                B--;
            }
        }
        if(A>N)
            A=1;
        else if(A<1)
            A=N;

        if(B>N)
            B=1;
        else if(B<1)
            B=N;
        printf("%d %d\n", A, B);
    }
}
