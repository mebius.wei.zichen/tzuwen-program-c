# How to Setup VS Code

```bash
sudo apt update -y
sudo apt install build-essential gdb gcc -y
sudo apt install snap -y
sudo snap install -classic code

g++ --version
gdb --version
```

![](images/2021-05-22-13-00-48.png)

![](images/2021-05-22-13-05-38.png)
