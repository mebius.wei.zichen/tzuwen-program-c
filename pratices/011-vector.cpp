#include <iostream>
#include <vector>
#include <exception>

using namespace std;

template <typename string>
void printVector(const vector<string> &vec)
{
    cout << "used: " << vec.size() << "/" << vec.capacity() << endl;

    for (const string &t : vec)
    {
        cout << t << " ";
    }

    cout << endl;
}

int main()
{
    vector<string> fruits;

    fruits.push_back("Apple");
    fruits.push_back("Banana");
    fruits.push_back("Cherry");
    fruits.insert(fruits.begin(), "Orange");
    fruits.insert(fruits.begin() + 2, "Drink");
    fruits.insert(fruits.end() - 1, "Drink");
    fruits.pop_back();

    //fruits.pop_back();

    printVector(fruits);

    return 0;
}
