#include <iostream>
#include <array>
#include <queue>

using namespace std;

int main()
{

    deque<int> myqueue;

    myqueue.push_front(1);
    myqueue.push_front(2);
    myqueue.push_back(5);
    myqueue.pop_front();
    // myqueue.push_back(800);

    for (unsigned int i = 0; i < myqueue.size(); i++)
    {
        cout << myqueue[i] << " ";
    }

    return 0;
}
