#include <iostream>
#include <array>

using namespace std;

template <size_t SIZE>
void fun(array<int, SIZE> &arr)
{
    cout << "array size: " << arr.size() << endl;

    for (int i = 0; i < arr.size(); i++)
        arr[i] = arr[i] + 10;
}

int main()
{
    array<int, 10> arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    array<int, 10> arr2 = {0};
    array<int, 10> arr3 = {0};

    // fun(arr2);

    arr2 = arr;

    // for (int x : arr)
    //     cout << x << endl;

    for (int x : arr2)
        cout << x << endl;

    return 0;
}
