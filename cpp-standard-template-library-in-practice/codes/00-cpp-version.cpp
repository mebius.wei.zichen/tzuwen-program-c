#include <iostream>

int main()
{
    std::cout << __cplusplus << std::endl;
}

/*
199711 for C++98
201103 for C++11
201402 for C++14
201703 for C++17

APCS: C++ Compiler: g++ -g -O2 -std=gnu++11 -static -lm -o <a.out>
NPSC: 編譯器：gcc 9.2.1 (Ubuntu 9.2.1-9ubuntu2~16.04.1)
*/