# CPP Version

## What is GCC?

GNU Compiler Collection, 因為它原本只能處理 C 語言。GCC 在发布后很快地得到擴展，變得可處理 C++

## APCS C++ Complier

Compiler: g++ -g -O2 -std=gnu++11 -static -lm -o <a.out>
Support: C++ 11


## NPCS

編譯器：gcc 9.2.1 (Ubuntu 9.2.1-9ubuntu2~16.04.1)

## Reference

- [APCS 系統環境](https://apcs.csie.ntnu.edu.tw/index.php/info/environment/)
- [NPSC 比賽環境](https://contest.cc.ntu.edu.tw/npsc2020/info.html)
